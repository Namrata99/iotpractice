def add(p1, p2):
    addition = p1 + p2
    print(f"{p1} + {p2} = {addition}")
def multiply(p1, p2):
    multiplication = p1 * p2
    print(f"{p1} * {p2} = {multiplication}")
if __name__ == '__main__':
    add(10, 20)
    multiply(3, 6)

