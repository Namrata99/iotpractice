#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266HTTPClient.h>

const char *ssid = "iPhone";
const char *password = "12345";

void setup() {
  Serial.begin(115200);
  Serial.flush();

  // set the pin mode
  pinMode(A0, INPUT);

  // STA: station
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  // wait till the connection gets successful
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.println("connecting...");
  }

  Serial.println("Connected... ");
  Serial.println("IP Address:");
  Serial.println(WiFi.localIP());
}

void loop() {
  // get the temperature
  float temperature = analogRead(A0);

  // get the json string
  String body = "{ \"temp\" : " + String(temperature) + "}";
  Serial.println("sending : " + body);

  // create the HTTP Client
  HTTPClient httpClient;

  // send the request
  httpClient.begin("http://172.20.10.3:4000/temp");
  httpClient.addHeader("Content-Type", "application/json");
  int statusCode = httpClient.POST(body);
  
  Serial.println("status code: " + String(statusCode));
  

  // add delay
  delay(1000);
}
