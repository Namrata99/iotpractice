def function1():
    numbers=list()
    print("type of numbers=",type(numbers))
function1()

def function2():
    numbers=[10,20,30,40]
    print("value at index 2nd=",numbers[2])
    print("value at index 1st=",numbers[1])
    print("value at index -2=",numbers[-2])
    print("value at index 4th=",numbers[3])
    print("slicing=",numbers[0:3])
    print("40 is present on index=",numbers.index(40))
function2()

def function3():
    countries["india","russia","usa","japan"]
    print(countries)
    countries.append("jermany")
    print(countries)
    country = countries.pop()
    print("popped value =", country)
    print(countries)
    country = countries.pop(1)
    print("popped value =", country)
    print(countries)
    countries.insert(2, "france")
    print(countries)
    print("number of values in countries =", len(countries))
    countries[2] = "Japan"
    print(countries)
function3()

def function4():
    persons = []

    # start: 0, stop: 10 (excluded), step: 1
    index_values = list(range(0, 3, 1))
    for index in index_values:
        # to get an input from user
        p1 = input("enter person name: ")
        persons.append(p1)

    print(persons)
function4()

def function5():
    # tuple
    t1 = (10, 20, 30, 40, 50)

    print("numbers in t1 =", len(t1))
    print(t1)
    del t1
function5()

def function6():
    person1 = ["person1", "person1@test.com", 30, True]
    print("name =", person1[0])
    print("email =", person1[1])
    print("age =", person1[2])
    print("eligible for voting =", person1[3])

    person2 = ("person2", 30, "person2@test.com", True)

    print("name =", person2[0])
    print("email =", person2[1])
    print("age =", person2[2])
    print("eligible for voting =", person2[3])

def function7():
    # dictionary:
    # - collection of key-value pairs
    # - where
    #   - every key has to be a string
    #   - a value can be of any data type
    person1 = {
        "name": "person1",
        "email": "person1@test.com",
        "age": 10,
        "canVote": False
    }

    # print("person1 =", person1)
    # print("type of person1 =", type(person1))

    print("name =", person1["name"])
    print("email =", person1["email"])
    print("age =", person1["age"])
    print("canVote =", person1["canVote"])

    if person1["canVote"] == True:
        print("Yes. The person is eligible for voting. :)")
    else:
        print("No. The person is NOT eligible for voting. :(")

    person2 = {
        "age": 60,
        "canVote": True,
        "email": "person2@test.com",
        "name": "person2"
    }

    print("name =", person2.get("name"))
    print("email =", person2.get("email"))
    print("age =", person2.get("age"))
    print("canVote =", person2.get("canVote"))

    if person2["canVote"] == True:
        print("Yes. The person is eligible for voting. :)")
    else:
        print("No. The person is NOT eligible for voting. :(")

    person2["name"] = "person3"
    print("name =", person2["name"])

    print("keys =", person1.keys())
    print("values =", person1.values())


# function7()


def function8():
    # list
    numbers_1 = [10, 20, 30, 40, 50, 10, 20, 30, 40, 50, 10, 20, 30, 40, 50]

    # tuple
    numbers_2 = (10, 20, 30, 40, 50, 10, 20, 30, 40, 50, 10, 20, 30, 40, 50)

    # set
    numbers_3 = {10, 20, 30, 40, 50, 10, 20, 30, 40, 50, 10, 20, 30, 40, 50}

    print(numbers_1)
    print("type of numbers_1 =", type(numbers_1))

    print(numbers_2)
    print("type of numbers_2 =", type(numbers_2))

    print(numbers_3)
    print("type of numbers_3 =", type(numbers_3))


# function8()


def function9():
    # list of dictionaries
    cars = [
        {"model": "i20", "company": "hyundai", "price": 7.5}, # 0
        {"model": "i10", "company": "hyundai", "price": 5.5}, # 1
        {"model": "nano", "company": "tata", "price": 1.5}    # 2
    ]

    # print(cars)
    # print("model =", cars[0])
    # print("model =", cars[1])
    # print("model =", cars[2])

    for car in cars:
        # print(car)
        print("model =", car["model"])
        print("company =", car.get("company"))
        print("price =", car["price"])
        print()

    print("company =", cars[2]["company"])
    print("company =", cars[2].get("company"))


function9()


def function10():
    # list of tuples
    # collection of collections => multi-dimensional collection
    mobiles = [
         # 0              1        2
        ("iphone xs max", "apple", 144000), # 0
        ("z10", "blackberry", 40000),       # 1
        ("galaxy s10", "samsung", 78000)    # 2
    ]

    # for mobile in mobiles:
    #     # print(mobile)
    #     print("model =", mobile[0])
    #     print("company =", mobile[1])
    #     print("price =", mobile[2])
    #     print()

    print("model = ", mobiles[0][0])


# function10()

