void setup() {
  Serial.begin(115200);
  Serial.flush();

  // select the required pin for required operation
  pinMode(D2, OUTPUT);
}

void loop() {
  
  // turn the LED ON
  digitalWrite(D2, HIGH);

  // add delay
  delay(2000);

  // turn the LED OFF
  digitalWrite(D2,LOW);

  // add delay
  delay(2000);
}
