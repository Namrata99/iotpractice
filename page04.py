def function1(p1):
    print("inside function1")
    print("p1 =", p1)
def function2(p1, p2):
    print("inside function2")
    print("p1 =", p1)
    print("p2 =", p2)
def function3(p1=0, p2=0):
    print("inside function3")
    print("p1 =", p1)
    print("p2 =", p2)
function3(10,20)
function3(10)
function3()