void setup() {
  Serial.begin(115200);
  Serial.flush();

  // select the pin for INPUT
  pinMode(A0, INPUT);
}

void loop() {
  // read temperature value from A0
  float temperature = analogRead(A0);
  Serial.print("Temperature: ");
  Serial.println(temperature);
  delay(2000);
  
}
