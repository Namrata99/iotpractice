void setup() {
  Serial.begin(115200);
  Serial.flush();

  // select the required pin for required operation
  pinMode(D0, OUTPUT);
}

void loop() {
  
  // turn the LED ON
  digitalWrite(D0, HIGH);

  // add delay
  delay(200);

  // turn the LED OFF
  digitalWrite(D0,LOW);

  // add delay
  delay(200);
  
}
