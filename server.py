from flask import Flask
from flask import jsonify
from flask import request
from flask import abort

app=Flask(__name__)
people=[{"id":"p1", "name":"Nilesh", "age":37, "addr":"Mumbai"},
        {"id":"p2", "name":"Devendra", "age":30, "addr":"Pune"},
        {"id":"p3", "name":"Abhijeet", "age":27, "addr":"Nagpur"},
        {"id":"p4", "name":"Satara", "age":26, "addr":"Satara"}
    ]

@app.route("/hello")
def hello_world():
    return "Hello, World!"


@app.route("/persons")
def get_all_person():
    return jsonify(people)

@app.route("/persons/<id>", methods=["GET"])
def get_person(id):
    for p in people:
        if p["id"] == id:
            return p
    return jsonify(None)


@app.route("/persons", methods=["POST"])
def add_person():
    if not request.json:
        abort(400)
    people.append(request.json)
    return {"status": "success"}
if __name__ == "__main__":
    app.run()
