colours=["red","blue","grey","pink","green"]
print("colours:",colours)
colours.append("grey")
print("adding grey to list:",colours)
colours.insert(2,"pink")
print("adding pink at index 2:",colours)
col=colours.pop()
print("removed last element of list ",col)
print("\n",colours)
col=colours.pop(1)
print("after removing element from index 1:",colours)

print("length of list colours",len(colours))
colours[2]="peach"
print("list after replacing magenta with peach",colours)